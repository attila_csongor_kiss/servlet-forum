<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>	
	<meta charset="UTF-8">
	<title>Logout Page</title>
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
</head>
<body>
<div class="login-card">
		<h1>You succesfully logged out.</h1>
		
<div class="login-help">
		<a href="Login">Back to Log in</a>
</div>
</div>
</body>
</html>