<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Register</title>
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
</head>
<body>
<div class="login-card">
	<h1>Register</h1>
	<form id="user_data" method="post" action="Register">
		<label for="user_name">User</label>
		<input name="user_name" type="text">
		<br>
		<label for="password">Password</label>
		<input name="password" type="password">
		<br>
		<input type="submit" value="Register" class="login login-submit"/>
	</form>
<div class="login-help">
	<a href="Login">Back to Log in</a>
</div>
</div>
</body>
</html>