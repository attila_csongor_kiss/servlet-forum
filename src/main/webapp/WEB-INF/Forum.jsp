<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="MyTaglib" uri="MyTaglib"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta charset="utf-8">
	<title>Forum</title>
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
</head>
<body>
<div class="forum-card">
	<h1>Forum</h1>
</div>
<div class="forum-table">	
	<table>
		<MyTaglib:foreach var="comment" items="${requestScope.comments}">
			<tr>
				<td class="table_user">${comment.userName} on ${comment.dateOfComment}</td>
				</tr>
				<tr>
				<td>${comment.commentText}</td>
			</tr>
		</MyTaglib:foreach>
	</table>
	<br>
</div>	
	<form method="post" action="Forum" class="forum-form">
		<textarea name="comment" cols="30" rows="5"></textarea>
		<br>
		<button type="submit" class="login login-submit" value="Send">Send</button>
	</form>

<div class="login-help">
	<span class="user_name">Signed in as ${requestScope.userName}</span>
	<a href="Logout" >Logout</a>
</div>
</body>
</html>