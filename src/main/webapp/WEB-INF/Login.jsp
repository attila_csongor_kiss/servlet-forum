<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>	
	<meta charset="UTF-8">
	<title>Login Page</title>
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
</head>
<body>
<div class="login-card">
	<h1>Login Page</h1>
	<form id="user_data" method="post" action="Login">
		<label for="user_name">User</label>
		<input name="user_name" type="text">
		<br>
		<label for="password">Password</label>
		<input name="password" type="password">
		<br>
		<input type="submit" value="Login" class="login login-submit" />
	</form>
<div class="login-help">
	<a href="Register">Register new account</a>
</div>
</div>
</body>
</html>