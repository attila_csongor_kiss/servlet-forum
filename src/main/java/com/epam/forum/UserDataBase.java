package com.epam.forum;

import java.util.ArrayList;
import java.util.List;

public class UserDataBase {
	
	private static UserDataBase instance = new UserDataBase();

	private final List<User> users;

	private UserDataBase() {
		users = new ArrayList<User>();
		users.add(new User("root", "root"));
	}

	public static UserDataBase getUserList() {
		return instance;
	}

	
	public boolean isValidPassword(String password) {
		return password != null;
	}
	
	public boolean isValidUserName(String userName) {
		return userName != null;
	}
	
	public boolean isUserNameAlreadyUsed(String userName) {		
		boolean ret = false;
		for (User user : users) {
			if (user.getUserName().equals(userName)) {
				ret = true;
			}
		}
		return ret;
	}
	
	public void add(User user) {
		users.add(user);
	}

	public boolean isExisting(User user) {
		return users.contains(user);
	}
}
