package com.epam.forum;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Register extends HttpServlet {
	private static final long serialVersionUID = 2099841814966287161L;
	private final UserDataBase users = UserDataBase.getUserList();

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/Register.jsp").forward(request,
				response);
	}

	@Override
	public void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException {
		final String userName = request.getParameter("user_name");
		final String password = request.getParameter("password");

		if (users.isValidUserName(userName) && users.isValidPassword(password)
				&& !users.isUserNameAlreadyUsed(userName)) {
			users.add(new User(userName, password));
			response.sendRedirect("Login");
		}
	}
}
