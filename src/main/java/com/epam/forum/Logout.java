package com.epam.forum;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Logout extends HttpServlet {  
	private static final long serialVersionUID = 5326532652562625555L;
	private static final String COOKIE_NAME = "OUT_REMEMBERME";
	private final Map<String, String> rememberMe = new HashMap<String, String>();
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response)  
                            throws ServletException, IOException {  
        response.setContentType("text/html"); 
        
        request.getRequestDispatcher("WEB-INF/Logout.jsp").include(request, response);
        
        for (Cookie c : request.getCookies()) {
			if (c.getName().equals(COOKIE_NAME)) {
				final String userName = rememberMe.get(c.getValue());
				rememberMe.remove(userName);
				c.setMaxAge(0);  
			}
		}
        
        final HttpSession session = request.getSession();
		synchronized (session) {						
			session.invalidate();  
		}
        
		
          
    }  
    
}  
